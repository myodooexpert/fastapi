#!/bin/bash
set -e
if [ -z $1 ]
then
  echo "You should input variable path"
else
  cd $1
  python3 -m venv venv
  touch Dockerfile
  touch docker-compose.yml
  touch .dockerignore
  touch .dockerenv
  touch .env
  touch .gitignore
  cat > main.py <<EOL
from fastapi import FastAPI
app = FastAPI()
@app.get("/")
def root():
  return "Hello World"
EOL
  mkdir app
  mkdir ./app/schema
  mkdir ./app/model
  mkdir ./app/repository
  mkdir ./app/api
  mkdir ./app/util
  mkdir ./app/setting
  mkdir ./app/database

  mkdir test
  touch ./test/__init__.py
  mkdir scripting
  cat > requirements.txt <<EOL
fastapi
uvicorn
pydantic
sqlalchemy
python-decouple
pytest
httpx
psycopg2
kafka-python
EOL
  source venv/bin/activate
  pip install -r requirements.txt
fi